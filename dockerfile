# pull official base image
FROM node:10-alpine

# set working directory
WORKDIR /usr/src/app

# install app dependencies
COPY app.js .
RUN npm init -y && npm install express --save

# start app
CMD node app.js
